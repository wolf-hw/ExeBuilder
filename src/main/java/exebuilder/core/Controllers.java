package exebuilder.core;

import javafx.fxml.Initializable;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class Controllers {

  private static final Logger logger = LogManager.getLogger();

  public static final Map<String, Initializable> controllers = new HashMap<>();

  public static void add(String key, Initializable controller) {
    controllers.put(key, controller);
    logger.debug("controller {}->{} added", key, controller);
  }

  public static Initializable get(String key) {
    return controllers.get(key);
  }

}
